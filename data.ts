const Home = () => {
  const data = {
    1: "home",
    2: "bed",
    3: "todo",
  };
  return data;
};
const Time = () => {
  const t = new Date();
  const data = { time: t.toLocaleTimeString() };
  return data;
};
export { Home, Time };
