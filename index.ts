import express from "express";
import { Home, Time } from "./data";

var response = {};
const App: express.Express = express();
App.use(express.static("public"));
App.use(express.json());

const Hello = (m: string) => {
  return m;
};

App.get("/", (_, res) => {
  res.send(Hello("Hello-World Typescript Express"));
});

App.get("/home", (_, res: express.Response) => {
  res.send(Home());
});
App.post("/",(req,res)=>{
  console.log(req.body)
  console.log(req.body.name)
  response = {name: req.body.name}
  res.send("POST REQUESTED")
} )

App.get("/home/time", (_, res: express.Response) => {
  res.send(Time());
});

App.get("/posts", (_, res: express.Response) => {
  res.send(response);
//curl -X POST http://localhost:8080/ -H "Accept: application/json" -H "Content-type: application/json" -d '{ "name" : "ro" }'
});

const port: number = 8080;

App.listen(port, "0.0.0.0", () => {
  console.log(Hello("Active in https://localhost:8080"));
});
